"""
This module encapsulates an Implicitome data auditor module
for the Knowledge.Bio application

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
from threading import Thread

from django.db.transaction import commit,rollback

# from django.db import connections
# print >>logger, connections.queries

from gbk.settings import logger
 
DEBUG  = False
TRACE  = False

from query.datasource import DSID
from query import QueryFactory

from service import ImplicitomeQuery

from implicitome.models import (
    Concept,
    Tuples,
)

"""
The auditor() method iterates through all of 
the tuples in "small" batches at a time, 
recording their concept_id's in a master dictionary 
later used to mark up the concept table orphan concepts.

July 28, 2015 Revisions:

1) Constrain on TOP_TENTH_PERCENTILE making the assumption that 
   database queries will only search for concepts in the top percentiles
   
2) Read in and parse one million rather than just ten thousand tuples at a time

"""
def auditor():
    
    print >>logger, "#### Starting Implicitome Data Auditor (IDA) ####"
    
    query_engine = QueryFactory( DSID.IMPLICITOME )
    
    # First, visit all the tuples(in small batch retrievals
    # from the database), noting the concept id's seen
    
    totalTuples = Tuples.objects.using( query_engine.getDsid() ).\
                    filter(percentile__gte=ImplicitomeQuery.TOP_TENTH_PERCENTILE).count()
                    
    print >>logger, "#### IDA:\tTotal number of Tuples is: ",  totalTuples
    
    if not DEBUG:
        batchsize = 1000000 # up from 10,000
    else:
        batchsize = 100
        
    n = 0
        
    tuples = None  
    conceptInTuples = {}

    for start in range( 0, totalTuples, batchsize ):  
        
        end = start + batchsize
        
        try:
            print >>logger, "#### IDA:\tReading Tuple records from ",    start, " to ", end-1,
            
            tuples = Tuples.objects.using( query_engine.getDsid() ).all()[ start : end ]
            
            print >>logger, "... parsing the tuples..."
            
            for entry in tuples:
                
                n += 1
                
                # look at a small subset only for a test run
                if DEBUG:
                    print >>logger, "#### IDA:\tFound Tuple '",    entry.tuple_id, \
                                    "' with subject '", entry.sub_name, \
                                    "' and object '",   entry.obj_name,"'"
        
                conceptInTuples[entry.sub_id] = 1
                conceptInTuples[entry.obj_id] = 1
                
                if n%100 == 0:
                    print >>logger,".", # progress dot...
                if n%2500 == 0:
                    print >>logger # newline every 25 dots
                if n%100000 == 0:
                    print >>logger, "#### IDA:\t", n , " tuples parsed so far..."
                    
            if DEBUG: 
                print >>logger, "...finished parsing batch: ",n," tuples parsed so far..."
                
        except Exception as e:
            print >>logger,"#### IDA:\tTuple table reading error: ", str(e)
            # gross data retrieval failure? but, 
            # could be unforeseen error at end of the parsing, 
            #so continue in case partially successful?
            break
        
        if DEBUG and n>=1000: break ;
        
    print >>logger, "#### IDA:\tTotal of ", n , " tuples interrogated."
    
    # ...then, mark all the concepts as orphan or not      
    totalConcepts = Concept.objects.using( query_engine.getDsid() ).all().count()
    print >>logger, "#### IDA:\tTotal number of Concepts is: ",  totalConcepts
    
    batchsize = 10000
    
    n = o = 0
    
    concepts = None
    
    for start in range( 0, totalConcepts, batchsize ):
        
        end = start + batchsize
        
        try:
            concepts = Concept.objects.using( query_engine.getDsid() ).all() [ start : end ]
            
        except Exception as e:
            print >>logger,"\n#### IDA:\t#### Concept table reading error: ", str(e)
            return  # gross data retrieval failure?
        
        for concept in concepts:
            
            n += 1
    
            if concept.concept_id in conceptInTuples:
                
                if DEBUG:
                    # For less prolific but useful log output in test situation, 
                    # simply report what concepts are NOT orphans,
                    # for comparison against the handful of original tuples read in.
                    print >>logger, "#### IDA:\tConcept '", concept.concept_id, "' with name '", concept.name, "' is NOT an orphan."
                    
                concept.is_orphan = 0
            else:
                if TRACE:
                    print >>logger, "#### IDA:\tFound Orphan Concept: ", concept.concept_id, " with name: ", concept.name
                    
                o += 1   
                concept.is_orphan = 1
    
            # save the updated record
            try:
                concept.save()
                commit( using = query_engine.getDsid() )
    
            except Exception as e:
                print >>logger,e # not sure what failure modes this script has...
                rollback( using= query_engine.getDsid() )
    
            if n%100 == 0:
                print >>logger,".", # progress dot...
            if n%2000 == 0:
                print >>logger # newline every 20 dots
            if n%100000 == 0:
                print >>logger, "#### IDA:\t", n , " concepts audited so far..."

    print >>logger, "#### IDA completed: ",o," orphans found among ",n," concepts in the database ###"
    
def data_audit():
    print >>logger, "#### Starting background thread for Implicitome Data Auditor (IDA) ####"
    
    Thread(
        target=auditor, 
        name="Implicitome_Data_Auditor", 
        kwargs={}
    ).start()
    
    print >>logger, "#### IDA:\tbackground thread started..."

