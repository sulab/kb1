# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('implicitome', '0002_concept_is_orphan'),
    ]

    operations = [
        migrations.CreateModel(
            name='TuplesMerged',
            fields=[
                ('tuple_id', models.AutoField(serialize=False, primary_key=True)),
                ('sub_id', models.IntegerField()),
                ('sub_name', models.CharField(max_length=100, blank=True)),
                ('obj_id', models.IntegerField()),
                ('obj_name', models.CharField(max_length=100, blank=True)),
                ('score', models.DecimalField(max_digits=20, decimal_places=16)),
                ('percentile', models.DecimalField(null=True, max_digits=12, decimal_places=10, blank=True)),
                ('linked_concepts', models.TextField(max_length=500, blank=True)),
            ],
            options={
                'db_table': 'tuples_merged',
                'managed': True,
            },
            bases=(models.Model,),
        ),
    ]
