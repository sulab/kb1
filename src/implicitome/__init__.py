# -*- coding: utf-8 -*-
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import sys

from django.utils import timezone

from query.datasource import DSID

from models import ( 
    Concept,
    Dblink,
    Relation,
    Term,
    Tuples,
)

class ImplicitomeTestData:
    CONCEPT1_UMLS = u'C0000189'
    CONCEPT1_EG1  = u'246730' # mouse 
    CONCEPT1_EG2  = u'4938'   # human
    CONCEPT1_OM   = u'164350'
    CONCEPT1_NAME = u'2-5a synthetase' 
    CONCEPT1_DEFINITION = u'An enzyme that catalyzes the conversion of ATP...' 
    CONCEPT1_CONCEPT_ID = None
    
    CONCEPT2_UMLS = u'C1234567'
    CONCEPT2_NAME = u'catalyzes' 
    CONCEPT2_DEFINITION = u'A process of accelerating a chemical reaction' 
    CONCEPT2_CONCEPT_ID = None
    
    CONCEPT3_UMLS = u'C0001480'
    CONCEPT3_NAME = u'Adenosine Triphosphate' 
    CONCEPT3_DEFINITION = u'Energy storage compound in living cells.' 
    CONCEPT3_CONCEPT_ID = None
    
    CONCEPT4_UMLS = u'C0001407'
    CONCEPT4_NAME = u'Adenine' 
    CONCEPT4_DEFINITION = u'Nucleotide base' 
    CONCEPT4_CONCEPT_ID = None
   
    CONCEPT5_UMLS = u'C1157223'
    CONCEPT5_NAME = u'ATP Biosynthesis' 
    CONCEPT5_DEFINITION = u'Process of synthesis of ATP' 
    CONCEPT5_CONCEPT_ID = None

    FIRST_MISSING_A_CONCEPT_ID="3063780"
    FIRST_MISSING_A_NAME="ALDH3A1"
    FIRST_MISSING_C_CONCEPT_ID="151449"
    FIRST_MISSING_C_NAME=u"primary sjögren's syndrome"
    FIRST_MISSING_LINKED_CONCEPTS=u"43352|Xerostomia|14.5231598383|314719|dryness of eye|6.32990135957|13238|dry eye syndromes|5.50340050567|1707516|Corneal Sensitivity|4.88618279119|455242|Covering eye|3.05354399702"
    FIRST_MISSING_ENTRY=\
        FIRST_MISSING_A_CONCEPT_ID+"|"+\
        FIRST_MISSING_A_NAME+"|"+\
        FIRST_MISSING_C_CONCEPT_ID+"|"+\
        FIRST_MISSING_C_NAME+"|"+\
        FIRST_MISSING_LINKED_CONCEPTS
    
    SECOND_MISSING_LINKED_CONCEPTS=u"521989|Associated symptom|52.7625999446|748972|SSA|16.138760485|3063611|LOC100294468|7.34927805389|702121|antinuclear antibody assay|2.61578447625|341439|chronic liver disease|1.27341370066"
    SECOND_MISSING_ENTRY=None
    
    CONCEPT6_NAME = u"ALDH2" 
    SECOND_MISSING_A_CONCEPT_ID = None
    
    CONCEPT7_NAME = u"in sjögren's syndrome keratoconjunctivitis sicca" 
    SECOND_MISSING_C_CONCEPT_ID = None
    
    TUPLE1 = None
    TUPLE2 = None
    TUPLE3 = None
    TUPLE4 = None
    TUPLE5 = None
    
    TUPLE4_LINKED_CONCEPTS = ""
   
def loadImplicitomeTestData():
    """
    Implicitome Test Query for unit testing of system
    """    
    
    dsid = DSID.IMPLICITOME
    
    print >>sys.stderr,"Executing loadImplicitomeTestData(",dsid,")"
    
    c1 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT1_NAME,
        definition = ImplicitomeTestData.CONCEPT1_DEFINITION
    )
    
    ImplicitomeTestData.CONCEPT1_CONCEPT_ID = c1.concept_id
    
    c2 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT2_NAME,
        definition = ImplicitomeTestData.CONCEPT2_DEFINITION
    )
    
    ImplicitomeTestData.CONCEPT2_CONCEPT_ID = c2.concept_id
    
    c3 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT3_NAME,
        definition = ImplicitomeTestData.CONCEPT3_DEFINITION
    )
    
    ImplicitomeTestData.CONCEPT3_CONCEPT_ID = c3.concept_id
    
    c4 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT4_NAME,
        definition = ImplicitomeTestData.CONCEPT4_DEFINITION
    )
    
    ImplicitomeTestData.CONCEPT4_CONCEPT_ID = c4.concept_id
    
    c5 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT5_NAME,
        definition = ImplicitomeTestData.CONCEPT5_DEFINITION
    )
    
    ImplicitomeTestData.CONCEPT5_CONCEPT_ID = c5.concept_id
    
    c6 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT6_NAME,
    )
    
    ImplicitomeTestData.SECOND_MISSING_A_CONCEPT_ID = c6.concept_id
    
    c7 = Concept.objects.using(dsid).create(
        name = ImplicitomeTestData.CONCEPT7_NAME,
    )
    
    ImplicitomeTestData.SECOND_MISSING_C_CONCEPT_ID = c7.concept_id
    
    dbl1 = Dblink.objects.using(dsid).create(
        concept_id  = c1.concept_id,
        db_id       = 'UMLS',
        identifier  = ImplicitomeTestData.CONCEPT1_UMLS 
    )
  
    Dblink.objects.using(dsid).create(
        concept_id  = c1.concept_id,
        db_id       = 'EG',
        identifier  = ImplicitomeTestData.CONCEPT1_EG1 
    )
    
    Dblink.objects.using(dsid).create(
        concept_id  = c1.concept_id,
        db_id       = 'EG',
        identifier  = ImplicitomeTestData.CONCEPT1_EG2 
    )
    
    Dblink.objects.using(dsid).create(
        concept_id  = c1.concept_id,
        db_id       = 'OM',
        identifier  = ImplicitomeTestData.CONCEPT1_OM
    )
    
    dbl2 = Dblink.objects.using(dsid).create(
        concept_id  = c2.concept_id,
        db_id       = 'UMLS',
        identifier  = ImplicitomeTestData.CONCEPT2_UMLS 
    )
    
    dbl3 = Dblink.objects.using(dsid).create(
        concept_id  = c3.concept_id,
        db_id       = 'UMLS',
        identifier  = ImplicitomeTestData.CONCEPT3_UMLS 
    )
    
    dbl4 = Dblink.objects.using(dsid).create(
        concept_id  = c4.concept_id,
        db_id       = 'UMLS',
        identifier  = ImplicitomeTestData.CONCEPT4_UMLS 
    )
    
    dbl5 = Dblink.objects.using(dsid).create(
        concept_id  = c5.concept_id,
        db_id       = 'UMLS',
        identifier  = ImplicitomeTestData.CONCEPT5_UMLS 
    )
    
    Relation.objects.using(dsid).create(
        concept1_id = c1.concept_id,
        concept2_id = c3.concept_id,
        relationtype_id = c2.concept_id
    )

    Term.objects.using(dsid).create(
        concept_id = c1.concept_id,
        subterm_id = 0,
        text      = c1.name,
        casesensitive  = 0,
        ordersensitive = 1,
        normalised     = 0
    )
    
    Term.objects.using(dsid).create(
        concept_id = c2.concept_id,
        subterm_id = 0,
        text      = c2.name,
        casesensitive  = 0,
        ordersensitive = 1,
        normalised     = 0
    )
    
    Term.objects.using(dsid).create(
        concept_id = c3.concept_id,
        subterm_id = 0,
        text      = c3.name,
        casesensitive  = 0,
        ordersensitive = 1,
        normalised     = 0
    )
    
    Term.objects.using(dsid).create(
        concept_id = c4.concept_id,
        subterm_id = 0,
        text      = c4.name,
        casesensitive  = 0,
        ordersensitive = 1,
        normalised     = 0
    )
    
    tpl1 = \
        Tuples.objects.using(dsid).create(
            sub_id   = c1.concept_id,
            sub_name = c1.name,
            obj_id   = c3.concept_id,
            obj_name = c3.name,
            score    = 0.0625,
            percentile = 90.0
        )
    ImplicitomeTestData.TUPLE1 = tpl1.tuple_id
        
    tpl2 = \
        Tuples.objects.using(dsid).create(
            sub_id   = c3.concept_id,
            sub_name = c3.name,
            obj_id   = c2.concept_id,
            obj_name = c2.name,
            score    = 0.125,
            percentile = 92.0
        )
    ImplicitomeTestData.TUPLE2 = tpl2.tuple_id
        
    tpl3 = \
        Tuples.objects.using(dsid).create(
            sub_id   = c3.concept_id,
            sub_name = c3.name,
            obj_id   = c4.concept_id,
            obj_name = c4.name,
            score    = 0.1875,
            percentile = 91.5
        )
    ImplicitomeTestData.TUPLE3 = tpl3.tuple_id

    ImplicitomeTestData.TUPLE4_LINKED_CONCEPTS = \
        "|".join([
                  str(ImplicitomeTestData.CONCEPT1_CONCEPT_ID),
                  ImplicitomeTestData.CONCEPT1_NAME,
                  "12.75",
                  
                  str(ImplicitomeTestData.CONCEPT4_CONCEPT_ID),
                  ImplicitomeTestData.CONCEPT4_NAME,
                  "2.25"
    ])
    
    tpl4 = \
        Tuples.objects.using(dsid).create(
            sub_id   = c3.concept_id,
            sub_name = c3.name,
            obj_id   = c5.concept_id,
            obj_name = c5.name,
            score    = 0.0575,
            percentile = 89.5,
            linked_concepts = ImplicitomeTestData.TUPLE4_LINKED_CONCEPTS
        )
    ImplicitomeTestData.TUPLE4 = tpl4.tuple_id
    
    tpl5 = \
        Tuples.objects.using(dsid).create(
            sub_id   = c6.concept_id,
            sub_name = c6.name,
            obj_id   = c7.concept_id,
            obj_name = c7.name,
            score    = 1.1,
            percentile = 99.5,
        )
    ImplicitomeTestData.TUPLE5 = tpl5.tuple_id

    ImplicitomeTestData.SECOND_MISSING_ENTRY=\
        unicode(str(ImplicitomeTestData.SECOND_MISSING_A_CONCEPT_ID),"utf-8")+"|"+\
        ImplicitomeTestData.CONCEPT6_NAME+"|"+\
        unicode(str(ImplicitomeTestData.SECOND_MISSING_C_CONCEPT_ID),"utf-8")+"|"+\
        ImplicitomeTestData.CONCEPT7_NAME+"|"+\
        ImplicitomeTestData.SECOND_MISSING_LINKED_CONCEPTS


