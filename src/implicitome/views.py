"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys
from os import path
import json

from django.http import HttpResponse
from django.template.response import TemplateResponse

from gbk.settings import PROJECT_PATH

DEBUG = False
TRACE = False
TEST  = False

from service   import ImplicitomeQuery, percentileLimit
from abcloader import abcLoad, abcLoad2, abcCount
from implicitome.data_auditor import data_audit
  
ABC_DATA_FILE_PATH = path.join(PROJECT_PATH,"static","data","abc_model_data_file")

def loadabc(request):
    
    abcLoad(ABC_DATA_FILE_PATH)
        
    return TemplateResponse(request, 'implicitome/loadabc.jade')

def loadabc2(request):
    
    abcLoad2(ABC_DATA_FILE_PATH)
    
    ctx = {
           'percentile' : ImplicitomeQuery.DEFAULT_PERCENTILE,
    }
    return TemplateResponse(request, 'implicitome/loadabc2.jade',ctx)

def loadabc2p(request,percentile):
    
    limit = percentileLimit(percentile)
    
    abcLoad2(ABC_DATA_FILE_PATH, limit)
        
    ctx = {
           'percentile' : percentile,
    }
    return TemplateResponse(request, 'implicitome/loadabc2.jade',ctx)

def bcount(request):
    
    abcCount(ABC_DATA_FILE_PATH)
        
    return TemplateResponse(request, 'implicitome/bcount.jade')

TEST_FILE_PATH = path.join(PROJECT_PATH,"static","data","abc_test_data")

def testloadabc(request):
    
    abcLoad(TEST_FILE_PATH)
        
    return TemplateResponse(request, 'implicitome/loadabc.jade')


# ideally, this view method can be polled by some URL using Javascript alarm and AJAX...
# Global problem of threadsafety, idempotency and reentrancy (need user sessions too?)
#def abcLoadStatus(request):
#    if abcLoadingActive():
#        msg = "Loading..."
#    else:
#        msg = "Completed!"
#    return HttpResponse(msg,"text/html")

from tempfile import mkstemp
from os import access,F_OK,R_OK
import codecs

def abcfileform(request):
    return TemplateResponse(request, 'implicitome/abcfileform.jade')
    
def loadabcfile(request):
    '''
     Method to upload a Knowledge.Bio ABC data file
    '''
    if request.method == 'POST':
        
        abc_file_name = request.POST.get('abc_file_name')
        abc_file_data = request.POST.get('abc_file_data')
        percentile    = request.POST.get('percentile', ImplicitomeQuery.DEFAULT_PERCENTILE)
        limit = percentileLimit(percentile)
        mode = request.POST.get('mode')
        
        # localfilepath points to the 
        # local temporary file caching the data
        
        localfd, localfilepath = mkstemp()
        
        cachefile = codecs.open(localfilepath, mode='w', encoding='utf-8')
        
        if DEBUG: 
            print >>sys.stderr,"cachefile: ", localfilepath
    
        cachefile.write( abc_file_data )
     
        cachefile.close()

        if localfilepath and \
                access(localfilepath, F_OK) and \
                access(localfilepath, R_OK):
            
            if mode == 'direct':
                # compute percentile limit based on user's selection of percentile
                abcLoad( localfilepath, limit, echo=True )
                
            elif mode == 'sql':
                # in-memory merge, tuples.sql file will be 
                # created in ../static/data/generated folder
                # and (currently) needs to be manually loaded(?)
                abcLoad2( localfilepath )
                
            else:
                return HttpResponse(json.dumps({'error' : "Unknown loadabcfile mode?" }),"application/json")
            
            ctx = {
                   "abc_file_name": abc_file_name,
                   "percentile" : percentile,
                   "mode" : mode,
            }                
            return TemplateResponse( request, 'implicitome/loadabc.jade', ctx )
        
        else:
            errmsg = "cachefile: ", localfilepath, " cannot be read?"
            print >>sys.stderr,errmsg
    else:
        errmsg = "Can't load a GET request?"

    return HttpResponse(json.dumps({'error' : errmsg }),"application/json")

def auditor(request):
    
    data_audit()
        
    return TemplateResponse(request, 'implicitome/audit.jade')
