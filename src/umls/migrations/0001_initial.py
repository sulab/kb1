# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Definition',
            fields=[
                ('definition_id', models.AutoField(serialize=False, primary_key=True, db_column='DEFINITION_ID')),
                ('cui', models.CharField(max_length=8, db_column='CUI')),
                ('aui', models.CharField(max_length=9, db_column='AUI')),
                ('atui', models.CharField(max_length=11, db_column='ATUI')),
                ('satui', models.CharField(max_length=50, null=True, db_column='SATUI')),
                ('sab', models.CharField(max_length=40, db_column='SAB')),
                ('definition', models.TextField(max_length=10000, db_column='DEF')),
                ('suppress', models.CharField(max_length=1, db_column='SUPPRESS')),
                ('cvf', models.IntegerField(max_length=10, null=True, db_column='CVF')),
            ],
            options={
                'db_table': 'MRDEF',
                'managed': True,
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='definition',
            unique_together=set([('cui', 'aui', 'sab')]),
        ),
    ]
