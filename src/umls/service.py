# -*- coding: utf-8 -*-
"""
This module encapsulates the UMLS query service module
for the Knowledge.Bio application

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
from gbk.settings import logger

import re

from django.db.transaction import commit, rollback
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

DEBUG = False   # diagnostics for code flow
DEBUG2 = True   # diagnostics for code flow
TRACE = False   # dump TRACE information if true
TEST  = False    # activate stub code

from query import KBQueryError
from query.datasource import DSID
from query.service import Query

from umls.models import (
    Definition,
    Semtype
)

from django.db import connections

SABPATTERN = re.compile(r"^[A-Za-z0-9_\-.]+$")
         
class UMLSQuery(Query):
    
    def __init__(self):
        Query.__init__(self, DSID.UMLS)
        
    def directQuery(self,query,*param):
        
        cursor = connections[self.getDsid()].cursor()
    
        if not param==None and  len(param)>0:
            if TRACE: print >>logger, "Executing directQuery('"+query+"' with parameters '"+str(param)+")"
            cursor.execute(query, param)
        else:
            if TRACE: print >>logger, "Executing directQuery('"+query+"')"
            cursor.execute(query)
    
        return cursor.fetchall()

    """
    Service to set a UMLS META concept definition.
    If a previous definition record exists, it is overwritten. 
    Otherwise, a new Definition record is created.
    """
    def setDefinitionByCUI( self, cui, aui, sab, new_definition):
        
        if TRACE: 
            print >>logger,"Entering UMLS.setDefinitionByCUI(cui: ",cui,", aui: ",aui,", sab: ",sab,", definition:\n",new_definition,")"
         
        # sanity check on the values of cui/aui/sab, which is also a
        # basic protection against SQL injections on these fields
        if cui == None or not cui.isalnum(): 
            raise KBQueryError("setDefinitionByCUI() error: null or invalid cui argument")
        cui = cui.strip()
        
        if aui == None or not aui.isalnum(): 
            raise KBQueryError("setDefinitionByCUI() error: null or invalid aui argument")
        aui = aui.strip()
        
        if sab == None: 
            raise KBQueryError("setDefinitionByCUI() error: null sab argument")
        sabmatch = SABPATTERN.match(sab) # can include hyphens too
        if not sabmatch:
            raise KBQueryError("setDefinitionByCUI() error: invalid sab argument")

        sab = sab.strip()
        
        try:
            # Search for a definition associated with the cui, aui and sab combination
            def_obj = Definition.objects.using( self.getDsid() ).get( cui=cui, aui=aui, sab=sab )
            old_definition     = def_obj.definition,cui,aui,sab  # could be empty string
            def_obj.definition = unicode(new_definition)
            def_obj.save()
            
            # commit the transaction
            commit(using=self.getDsid())
            
        except ObjectDoesNotExist:
            Definition.objects.using( self.getDsid() ).create(
                cui = cui,
                aui = aui,
                sab = sab,
                definition = unicode( new_definition )
            )
            
            # commit the transaction
            commit(using=self.getDsid())
            
            old_definition = None # old definition didn't exist
            
        except MultipleObjectsReturned:
            rollback( using=self.getDsid() )
            raise KBQueryError("setDefinitionByCUI() error: multiple definitions matching cui: "+cui+", aui: "+aui+", sab: "+sab)

        # validation retrieval
        try:
            # Search for the definition associated with the cui, aui and sab combination
            def_obj = Definition.objects.using( self.getDsid() ).get( cui=cui, aui=aui, sab=sab )

            if def_obj.definition != unicode(new_definition):
                raise KBQueryError("setDefinitionByCUI() error: definition miss-match - returned: "+\
                                    unicode( def_obj.definition)+" instead of new: "+\
                                    unicode(new_definition)) 

        except ObjectDoesNotExist:
            raise KBQueryError("setDefinitionByCUI() error: missing the definition matching cui: "+cui+", aui: "+aui+", sab: "+sab)
            
        except MultipleObjectsReturned:
            raise KBQueryError("setDefinitionByCUI() error: multiple definitions matching cui: "+cui+", aui: "+aui+", sab: "+sab)
        
        return old_definition
    
    """
    Service to find CUI associated UMLS META concept definitions 
    """
    def getDefinitionsByCUI(self,cui):
        
        if TRACE: 
            print >>logger,"Entering UMLS.getDefinitionsByCUI(",cui,")"
         
        # sanity check on the values of cui, which is also a
        # basic protection against SQL injections on these fields
        if cui == None or not cui.isalnum(): 
            raise KBQueryError("getDefinitionByCUI() error: null or invalid cui argument")
        cui = cui.strip()
        
        # Search for a Definition record associated with the cui
        try:
            # Search for any definition associated with the cui
            results = Definition.objects.using( self.getDsid() ).filter( cui__exact = cui )

            if len(results) == 0: return (None,None)
            
            definitions = {}
            for entry in results:
                definitions[entry.sab] = entry.sab, entry.definition

            if "NCI_NCI-GLOSS" in definitions:
                return definitions["NCI_NCI-GLOSS"]
            elif "NCI" in definitions:
                return definitions["NCI"]
            elif "MSH" in definitions:
                return definitions["MSH"]
            elif "CSP" in definitions:
                return definitions["CSP"]
            else:
                return results[0].sab, results[0].definition
        
        except Exception as e:
            raise KBQueryError("getDefinitionsByCUI() error matching CUI: "+cui+": "+str(e))

    """
    Service to find CUI associated UMLS META concept definitions 
    """
    def getSemtypesByCUI(self,cui):
        
        if DEBUG: 
            print >>logger,"Entering UMLS.getSubtypeByCUI(",cui,")"
         
        # sanity check on the values of cui, which is also a
        # basic protection against SQL injections on these fields
        if cui == None or not cui.isalnum(): 
            raise KBQueryError("getSemtypeByCUI() error: null or invalid CUI argument")
        cui = cui.strip()
        
        # Search for a MRSTY concept semtypes record associated with the CUI
        try:
            # Search for any definition associated with the cui
            results = Semtype.objects.using( self.getDsid() ).filter( cui__exact = cui )

            if len(results) == 0: return ()
            
            subtypes = [ entry.tui for entry in results ]
            
            if DEBUG:
                print >>logger,"...returning ", subtypes
                
            return subtypes
        
        except Exception as e:
            raise KBQueryError("getSemtypeByCUI() error matching CUI: "+cui+": "+str(e))
