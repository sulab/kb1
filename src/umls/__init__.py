#!/usr/local/bin/python
# coding: utf-8
"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys

from django.utils import timezone

from query.datasource import DSID

from models import ( 
    Definition,
    Semtype,
)

class UMLSTestData:
    
    UMLS_CONCEPT1_CUI = u'C0051334'
    UMLS_CONCEPT1_TUI1 = u'T192'
    UMLS_CONCEPT1_SEMTYPE1 = u'Receptor'
    UMLS_CONCEPT1_TUI2 = u'T116'
    UMLS_CONCEPT1_SEMTYPE2 = u'Amino Acid, Peptide, or Protein'
    UMLS_CONCEPT1_AUI = u'A22791837'
    UMLS_CONCEPT1_SAB = u'MSH'
    UMLS_CONCEPT1_DEFINITION = \
u'''A member of the NICOTINIC ACETYLCHOLINE RECEPTOR subfamily of the 
LIGAND-GATED ION CHANNEL family. It consists entirely of pentameric α7 
subunits expressed in the CNS, autonomic nervous system, vascular system, 
lymphocytes and spleen.'''
    
    UMLS_CONCEPT1_DEFINITION_ID = None
    UMLS_CONCEPT1_SEMTYPE_ID    = None
    
    UMLS_CONCEPT2_CUI = u'C0598608'
    UMLS_CONCEPT2_AUI = u'A1373129'
    UMLS_CONCEPT2_SAB = u'MSH'
    UMLS_CONCEPT2_DEFINITION = \
u'''Condition in which the plasma levels of homocysteine and 
related metabolites are elevated (>13.9 μmol/l). Hyperhomocysteinemia 
can be familial or acquired. Development of the acquired hyperhomocysteinemia 
is mostly associated with vitamins B and/or folate deficiency 
(e.g., PERNICIOUS ANEMIA, vitamin malabsorption). Familial hyperhomocysteinemia 
often results in a more severe elevation of total homocysteine and excretion 
into the urine, resulting in HOMOCYSTINURIA. Hyperhomocysteinemia is a 
risk factor for cardiovascular and neurodegenerative diseases, 
osteoporotic fractures and complications during pregnancy.'''

    UMLS_CONCEPT3_CUI = u'C0598608'
    UMLS_CONCEPT3_AUI = u'A17692149'
    UMLS_CONCEPT3_SAB = u'NCI'
    UMLS_CONCEPT3_DEFINITION = \
u'''A serious metabolic condition caused by mutations in the MTHFR gene, 
medications, or nutritional deficiency. It results in increased levels of 
homocysteine in the blood. Patients with this condition are at an increased 
risk for recurrent blood clots formation and cardiovascular accidents.'''

    UMLS_CONCEPT4_CUI = u'C1157223'
    UMLS_CONCEPT4_AUI = u'A1157223'
    UMLS_CONCEPT4_SAB = u'MSH'
    UMLS_CONCEPT4_DEFINITION = u'Process of synthesis of ATP' 
  
    UMLS_CONCEPT5_CUI = u'C3227511'
    UMLS_CONCEPT5_AUI = u'A987654'
    UMLS_CONCEPT5_SAB = u'NCI'
    UMLS_CONCEPT5_DEFINITION = u'Another process of synthesis of ATP' 
  
def loadUMLSTestData():
    """
    UMLS Test Query for unit testing of system
    """    
    
    dsid = DSID.UMLS
    
    print >>sys.stderr,"Executing loadUMLSTestData(",dsid,")"
    
    Definition.objects.using(dsid).create(
        cui = UMLSTestData.UMLS_CONCEPT1_CUI,
        aui = UMLSTestData.UMLS_CONCEPT1_AUI,
        sab = UMLSTestData.UMLS_CONCEPT1_SAB,
        definition = UMLSTestData.UMLS_CONCEPT1_DEFINITION
    )
    
    Semtype.objects.using(dsid).create(
        cui  = UMLSTestData.UMLS_CONCEPT1_CUI,
        tui  = UMLSTestData.UMLS_CONCEPT1_TUI1,
        stn  = "A1.4.1.1.3.6",
        semtype  = UMLSTestData.UMLS_CONCEPT1_SEMTYPE1,
        atui = "AT17616194",
        cvf  = 256
    )

    Semtype.objects.using(dsid).create(
        cui  = UMLSTestData.UMLS_CONCEPT1_CUI,
        tui  = UMLSTestData.UMLS_CONCEPT1_TUI2,
        stn  = "A1.4.1.2.1.7",
        semtype  = UMLSTestData.UMLS_CONCEPT1_SEMTYPE2,
        atui = "AT17650107",
        cvf  = 256
    )

    Definition.objects.using(dsid).create(
        cui = UMLSTestData.UMLS_CONCEPT2_CUI,
        aui = UMLSTestData.UMLS_CONCEPT2_AUI,
        sab = UMLSTestData.UMLS_CONCEPT2_SAB,
        definition = UMLSTestData.UMLS_CONCEPT2_DEFINITION
    )

    Definition.objects.using(dsid).create(
        cui = UMLSTestData.UMLS_CONCEPT3_CUI,
        aui = UMLSTestData.UMLS_CONCEPT3_AUI,
        sab = UMLSTestData.UMLS_CONCEPT3_SAB,
        definition = UMLSTestData.UMLS_CONCEPT3_DEFINITION
    )

    Definition.objects.using(dsid).create(
        cui = UMLSTestData.UMLS_CONCEPT4_CUI,
        aui = UMLSTestData.UMLS_CONCEPT4_AUI,
        sab = UMLSTestData.UMLS_CONCEPT4_SAB,
        definition = UMLSTestData.UMLS_CONCEPT4_DEFINITION
    )


