"""
This module converts UMLS semantic type and group definitions 
into a form usable by Knowledge.Bio for semantic filtering

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import sys
import json

DEBUG = False   # diagnostics for code flow
TRACE = False   # dump DEBUG information if true
TEST  = False   # activate stub code

type_catalog = {}

def loadTypeEntry(line):
   
    entry = line.split("|")
    
    type_code  = entry[0]
    type_id    = entry[1]
    type_label = entry[2]
    
    if not type_id in type_catalog:
        type_catalog[type_id] = {}
        type_catalog[type_id]['code'] = type_code
        type_catalog[type_id]['name'] = type_label
    else:
        print sys.stderr, "loadTypeEntry(): Duplicate code = ",type_code," with label = ",type_label

group_catalog = {}

def loadGroupEntry(line):
   
    entry = line.split("|")
    
    group_code = entry[0]
    group_name = entry[1]
    type_id    = entry[2]
    type_label = entry[3]
    
    if not group_code in group_catalog:
        group_catalog[group_code] = {}
        group_catalog[group_code]['name'] = group_name
        group_catalog[group_code]['types'] = {}
    
    code = type_catalog[type_id]['code']
    if not code in group_catalog[group_code]['types']:
        
        if not type_label == type_catalog[type_id]['name']:
            print sys.stderr,   "Inconsistent label = ",type_label,\
                                "for type id = ",type_id,\
                                "... should be: ", type_catalog[type_id]['name']
            return
                    
        group_catalog[group_code]['types'][code] = {}
        group_catalog[group_code]['types'][code]['subtype'] = type_id
        group_catalog[group_code]['types'][code]['name']    = type_label
        
    else:
        print sys.stderr, "loadGroupEntry(): Duplicate code = ",code," with label = ",type_label

def loadTypes(filename):
    try:
        with open(filename,'r') as datafile:
            for line in datafile:
                line = line.strip()
                loadTypeEntry(line)
    except IOError as ioe:
        print >>sys.stderr,ioe

def loadGroups(filename):
    try:
        with open(filename,'r') as datafile:
            for line in datafile:
                line = line.strip()
                loadGroupEntry(line)
    except IOError as ioe:
        print >>sys.stderr,ioe

"""
Needs to be dumped in the following structure

{
  "types":[
              {
                "id": 0,
                "type":"group",
                "code": "ACTI",
                "name": "Activities & Behaviors",
                "children":[0,1,2,3,4,5,6,7,8]
              },
      ...
      ],
        "subtypes":[
            {
              "id": 0,
              "code": "aapp",
              "subtype": "T116",
              "name": "Amino Acid,  Peptide, or Protein"
            },
      ...
      ],
}
"""
def dumpSemanticTypes():
    
    types = []
    subtypes = []
    gid = 0
    tid = 0
    
    for group_code in sorted(group_catalog.keys()):
        
        gp = group_catalog[group_code]

        children = []
        for code in sorted(gp["types"].keys()):
            st = gp["types"][code]
                        
            stentry = {}
            stentry['id']      = tid
            children.append(tid)
            tid += 1
            stentry['code']    = code
            stentry['subtype'] = st['subtype']
            stentry['name']    = st['name']
            
            subtypes.append(stentry)
        
        gpentry = {}
        gpentry['id']      = gid
        gid += 1
        gpentry['type']     = "group"
        gpentry['code']     = group_code
        gpentry['name']     = gp['name']
        gpentry['children'] = children
            
        types.append(gpentry)
        
    output = { "types": types, "subtypes": subtypes }
    
    output = json.dumps(output)
    
    print >>sys.stderr, "catalog:\n",output

if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        '-t','--types', 
        metavar='<type_file>',required=True,
        help='Text file containing UMLS semantic types'
    )

    parser.add_argument(
        '-g','--groups', 
        metavar='<group_file>',required=True,
        help='Text file containing UMLS semantic type groups'
    )

    args = parser.parse_args()
    
    if args.types and args.groups:
        loadTypes(args.types)
        loadGroups(args.groups)
        dumpSemanticTypes()

    else:
        parser.help()