from django.template.response import TemplateResponse

import sys
import json

# Does python propagate importing dependencies?
from semmeddb    import loadSemMedDbTestData
from implicitome import loadImplicitomeTestData

from graph import testdata

DEBUG=True

def home(request):
    return TemplateResponse(request, 'graph/home.jade')

def representation(request):

    concept_id = request.POST.get('concept_id')
    relations  = request.POST.get('relations',0)
    
    if DEBUG:
        print >>sys.stderr,\
            "graph.views.representation("+\
            " concept_id = "+str(concept_id)+\
            ", relations = "+str(relations)+\
            "' )"
    
    graphData = json.dumps(testdata)
    
    # Call user session parameter object
    ctx = {
        'graphData' : graphData,
        'UserExpandCount': 0.5,
        'UserWeightCutoff': 5,
        'UserDataSource':"",
    }
    return TemplateResponse(request,'graph/rep.jade', ctx)

def test(request):
    return TemplateResponse(request, 'graph/test.jade')

def dragtest(request):
    return TemplateResponse(request, 'graph/dragtest.jade')

def loadTestDataForGraph( request ):
    loadSemMedDbTestData()
    loadImplicitomeTestData()
    return "Loaded"