/*
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
 * Knowledge.Bio - Utility Data Table library 
 */

function conceptRenderer( relation_type, resource ) {
    /* Using closure here to affect the data used for linkage/display */
    return function ( row, type, val, meta ) {
        var item ;
        if(resource == 'concept') {
            item = row ;
        } else {
            item = row[resource] ;
        }

        if (type == 'display') {
            var concept_id = item.concept_id.toString().trim() ;
            if ( concept_id.length>0 ) {
                
                var link = "<a href='/query/concept/"+relation_type+"/"+concept_id+"'";
                if( !( relation_type == 'implicit' ||
                       relation_type == 'linked' ) ){
                    link += " title='Type: "+item.semtype+"'";
                }
                
                link += ">"+item.name+"</a>";
                return link ; 

            } else {
                /* Try going to Unigene instead? */
                return "<a href='http://www.ncbi.nlm.nih.gov/unigene/?term="+item.name+
                       "' title='Type: "+item.semtype+"'  target='unigene'>"+item.name+"</a>";   
            }
        } else {
            /* 'filter', 'sort', 'type' and undefined all just use the name string */
            return item.name;
        }
    } ;
}

function evidenceRenderer() {
    return function ( row, type, val, meta ) {
        if (type == 'display') {
            var evidence_count = parseInt(row.evidence_count) ;
            if ( ! isNaN(evidence_count) ) {
                var predicate = row.subject.name+" "+row.predicate+" "+row.object.name ;
                var uri = encodeURI("/query/sentences/"+row.predication_id+"?predicate="+predicate) ;
                return "<a class='btn btn-default' href='"+uri+"'>"+evidence_count+"</a>";
            } else {
                return "" ;
            }
        } else {
          /* 'filter', 'sort', 'type' and 
             undefined are meaningless in 
             this context: just return blank */
          return "";
        }
    } ;
}

function visualizationCheckBox() {
    return function ( row, type, val, meta ) {
        if (type == 'display') {
          return "<form action=''><input type='checkbox'/></form>" ;
        } else {
          /* 'filter', 'sort', 'type' and 
             undefined are meaningless in 
             this context: just return blank */
          return "";
        }
    } ;
}

function citationRenderer() {
    return function ( row, type, val, meta ) {
        if (type == 'display') {
          return "<a href='/query/citation/"+row.pmid+"' title='PMID: "+row.pmid+"' target='KB_Citations'>"+row.sentence+"</a>";
        } else {
          /* 'filter', sort', 'type' and undefined, just return the sentence */
          return row.sentence;
        }
    } ;
}

function drawTable(mode) {
    /* Note that the number of column 'thead' and 'tfoot' should 
       be strictly equal to the number of 'data' columns specified
       for the 'columns' variable set in the ready() method which follows */
    var output =  "<table id='result_table' class='display table-condensed'>" ;
    var rowspec ;
    if( mode=='text' || mode=='exptext' || mode == 'imptext' ) {
        rowspec = "<th>Concept</th>" ;
    } else if( mode == 'cui' || mode == 'explicit' ) {
        rowspec  = "<th>Subject</th>"+
                   "<th>Relation</th>"+
                   "<th>Object</th>"+
                   "<th>Evidence</th>" ;
    } else if( mode == 'aliases' || mode=='implicit' ) {
        rowspec  = "<th>Subject</th>"+
                   "<th>Relation</th>"+
                   "<th>Object</th>"+
                   "<th>Score</th>" ;
    } else if(mode=='pmid') {
        rowspec = "<th>Subject</th>"+
                   "<th>Relation</th>"+
                   "<th>Object</th>"+
                   "<th>Other Citations</th>" ;
    } else if(mode=='sentences') {
        rowspec = "<th>Sentence</th>" ;
    } else {
        return 'ERROR: Unknown data query mode: '+ mode ;
    }
    output += "<thead><tr>"+rowspec+"</tr></thead>" ;
    output += "<tfoot><tr>"+rowspec+"</tr></tfoot>" ;
    output += "</table>" ;
    return output ;
}

$(document).ready( function () {

    var titleStr ;
    
    /* see comment in 'draw table' above */
    var columns = [] ;
    var orders  = [] ;
    if( mode=='text' ) {
        titleStr = "SemMedDb Concepts matching '"+name+"'" ;
        columns.push( { "data": null , "render": conceptRenderer("discovery","concept") } ) ;
        orders.push([0,'asc']) ;
        
    } else if( mode=='exptext' ) {
        titleStr = "SemMedDb Concepts matching '"+name+"'" ;
        columns.push( { "data": null , "render": conceptRenderer("explicit","concept") } ) ;
        orders.push([0,'asc']) ;
        
    } else if( mode == 'imptext' ) {
        titleStr = "Implicitome Concepts matching '"+query+"'" ;
        columns.push( { "data": null , "render": conceptRenderer("implicit","concept") } ) ;
        orders.push([0,'asc']) ;
        
     } else if( mode == 'cui' || mode == 'explicit' ) {
        titleStr = "SemMedDb Concept '"+name+"'" ;
        columns.push( { "data": null , "render": conceptRenderer("explicit","subject"), "orderable": false } ) ; 
        columns.push( { "data": "predicate", "orderable": false } ) ; 
        columns.push( { "data": null , "render": conceptRenderer("explicit","object"), "orderable": false } ) ;
        /*  columns.push( { "data": "score", "orderable": false } ) ; // no score in this table  */
        columns.push( { "data": null , "render": evidenceRenderer(), "orderable": false, "searchable": false } ) ; 
        /* columns.push( { "data": null , "render": visualizationCheckBox(), "orderable": false, "searchable": false } ) ; */
      
     } else if(  mode == 'aliases' || mode == 'implicit' ) {
        titleStr = "Implicitome Concept '"+name+"'" ;
        columns.push( { "data": null , "render": conceptRenderer("linked","subject"), "orderable": false } ) ; 
        columns.push( { "data": "predicate", "orderable": false, "searchable": false } ) ; 
        columns.push( { "data": null , "render": conceptRenderer("linked","object"), "orderable": false } ) ;
        columns.push( { "data": "score" } ) ; orders.push([3,'desc']) ;  /* order results initially from high to low score */
        /* columns.push( { "data": null , "render": evidenceRenderer(), "orderable": false, "searchable": false } ) ; // no evidence in this table */
        /* columns.push( { "data": null , "render": visualizationCheckBox(), "orderable": false, "searchable": false } ) ; */
      
    } else if(mode=='pmid') {
        titleStr = "Pubmed ID '"+name+"'" ;
        /* Can't yet figure out how to 
           allow ordering of results based on 
           subject, predicate or object fields, 
           so this DT feature is disabled for now;
           I'm also limiting searching on 
           subject and object fields (only) for now,
           again, for technical reasons...
         */
        columns.push( { "data": null , "render": conceptRenderer("explicit","subject"), "orderable": false } ) ; 
        columns.push( { "data": "predicate", "orderable": false } ) ; 
        columns.push( { "data": null , "render": conceptRenderer("explicit","object"), "orderable": false } ) ; 
        columns.push( { "data": null , "render": evidenceRenderer(), "orderable": false, "searchable": false } ) ; 
        
    } else if(mode=='sentences') {
        titleStr = "Predication '"+name+"'" ;
        columns.push( { "data": null , "render": citationRenderer() } ) ;
        orders.push([0,'asc']) ;
        
    } else {
        titleStr = "Unknown Search Type?" ;
    }

    /* alert("TitleStr:"+titleStr) ; alert( JSON.stringify(columns) ) ; */
    
    $('#result_title').append(titleStr) ;
    
    $('#result_div').append( drawTable(mode) ) ;
    
    var table = $('#result_table').DataTable(  {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/query/data',
            "type": 'POST',
            "data": { 
                'mode'  : mode, 
                'query' : query,
                'aliases' : aliases,
                'csrfmiddlewaretoken' : csrf_token
            },
        },
        /* "columnDefs" : [{ "width": "20%", "targets": 0 }], */
        "columns"    : columns,
        "order"      : orders,
    } );
    
    table.$('input, select').serialize();
} );
