"""
The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Concept(models.Model):
    concept_id = models.AutoField(db_column='CONCEPT_ID', primary_key=True)
    cui = models.CharField(db_column='CUI', max_length=20)
    type = models.CharField(db_column='TYPE', max_length=10)
    preferred_name = models.CharField(db_column='PREFERRED_NAME', max_length=200)
    ghr = models.CharField(db_column='GHR', max_length=250, blank=True)
    omim = models.CharField(db_column='OMIM', max_length=1000, blank=True)
    is_orphan = models.BooleanField(db_column="IS_ORPHAN",default=0)
    
    class Meta:
        managed = True
        db_table = 'CONCEPT'

class ConceptSemtype(models.Model):
    concept_semtype_id = models.AutoField(db_column='CONCEPT_SEMTYPE_ID', primary_key=True)
    concept = models.ForeignKey(Concept, db_column='CONCEPT_ID')
    semtype = models.CharField(db_column='SEMTYPE', max_length=4)
    novel = models.CharField(db_column='NOVEL', max_length=1)
    umls = models.CharField(db_column='UMLS', max_length=1, blank=True)

    class Meta:
        managed = True
        db_table = 'CONCEPT_SEMTYPE'

class Predication(models.Model):
    predication_id = models.AutoField(db_column='PREDICATION_ID', primary_key=True)
    predicate = models.CharField(db_column='PREDICATE', max_length=50)
    type = models.CharField(db_column='TYPE', max_length=10)

    class Meta:
        managed = True
        db_table = 'PREDICATION'

class PredicationArgument(models.Model):
    predication_argument_id = models.AutoField(db_column='PREDICATION_ARGUMENT_ID', primary_key=True)
    predication = models.ForeignKey(Predication, db_column='PREDICATION_ID')
    concept_semtype = models.ForeignKey(ConceptSemtype, db_column='CONCEPT_SEMTYPE_ID')
    type = models.CharField(db_column='TYPE', max_length=1)

    class Meta:
        managed = True
        db_table = 'PREDICATION_ARGUMENT'


class Sentence(models.Model):
    sentence_id = models.AutoField(db_column='SENTENCE_ID', primary_key=True)
    pmid = models.CharField(db_column='PMID', max_length=20)
    type = models.CharField(db_column='TYPE', max_length=2)
    number = models.IntegerField(db_column='NUMBER')
    sentence = models.CharField(db_column='SENTENCE', max_length=999)

    class Meta:
        managed = True
        db_table = 'SENTENCE'


class SentencePredication(models.Model):
    sentence_predication_id = models.AutoField(db_column='SENTENCE_PREDICATION_ID', primary_key=True)
    sentence = models.ForeignKey(Sentence, db_column='SENTENCE_ID')
    predication = models.ForeignKey(Predication, db_column='PREDICATION_ID')
    predication_number = models.IntegerField(db_column='PREDICATION_NUMBER')
    subject_text = models.CharField(db_column='SUBJECT_TEXT', max_length=200, blank=True)
    subject_dist = models.IntegerField(db_column='SUBJECT_DIST', blank=True, null=True)
    subject_maxdist = models.IntegerField(db_column='SUBJECT_MAXDIST', blank=True, null=True)
    subject_start_index = models.IntegerField(db_column='SUBJECT_START_INDEX', blank=True, null=True)
    subject_end_index = models.IntegerField(db_column='SUBJECT_END_INDEX', blank=True, null=True)
    subject_score = models.IntegerField(db_column='SUBJECT_SCORE', blank=True, null=True)
    indicator_type = models.CharField(db_column='INDICATOR_TYPE', max_length=10, blank=True)
    predicate_start_index = models.IntegerField(db_column='PREDICATE_START_INDEX', blank=True, null=True)
    predicate_end_index = models.IntegerField(db_column='PREDICATE_END_INDEX', blank=True, null=True)
    object_text = models.CharField(db_column='OBJECT_TEXT', max_length=200, blank=True)
    object_dist = models.IntegerField(db_column='OBJECT_DIST', blank=True, null=True)
    object_maxdist = models.IntegerField(db_column='OBJECT_MAXDIST', blank=True, null=True)
    object_start_index = models.IntegerField(db_column='OBJECT_START_INDEX', blank=True, null=True)
    object_end_index = models.IntegerField(db_column='OBJECT_END_INDEX', blank=True, null=True)
    object_score = models.IntegerField(db_column='OBJECT_SCORE', blank=True, null=True)
    curr_timestamp = models.DateTimeField(db_column='CURR_TIMESTAMP')

    class Meta:
        managed = True
        db_table = 'SENTENCE_PREDICATION'

class Citations(models.Model):
    pmid = models.CharField(db_column='PMID', primary_key=True, max_length=20)
    issn = models.CharField(db_column='ISSN', max_length=10, blank=True)
    dp = models.CharField(db_column='DP', max_length=50, blank=True)
    edat = models.CharField(db_column='EDAT', max_length=50, blank=True)
    pyear = models.IntegerField(db_column='PYEAR', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'CITATIONS'
        
"""        
class PredicationAggregate(models.Model):
    # new auto incrementing primary key field
    # needs to be added to the schema - 
    # but would be problematic for semmeddb updates?
    predication_aggregate_id = models.AutoField()  
    pid = models.IntegerField(db_column='PID')  
    sid = models.IntegerField(db_column='SID')  
    pnumber = models.IntegerField(db_column='PNUMBER')  
    pmid = models.CharField(db_column='PMID', max_length=20, blank=True)  
    predicate = models.CharField(max_length=50, blank=True)
    s_cui = models.CharField(max_length=255, blank=True)
    s_name = models.CharField(max_length=999, blank=True)
    s_type = models.CharField(max_length=50, blank=True)
    s_novel = models.IntegerField(blank=True, null=True)
    o_cui = models.CharField(max_length=255, blank=True)
    o_name = models.CharField(max_length=999, blank=True)
    o_type = models.CharField(max_length=50, blank=True)
    o_novel = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PREDICATION_AGGREGATE'
        unique_together = (('pid','sid'),) # new constraint?
"""

class ConceptCitations(models.Model):
    concept_citations_id = models.AutoField(db_column='CONCEPT_CITATIONS_ID', primary_key=True)  
    cui = models.CharField(db_column='CUI', max_length=20,unique='True')  
    pmid = models.TextField(db_column='PMID')  

    class Meta:
        managed = True
        db_table = 'CONCEPT_CITATIONS'
