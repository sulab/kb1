"""
This module encapsulates a SemMedDb data auditor module
for the Knowledge.Bio application

The MIT License (MIT)

Copyright (c) 2015 Scripps Institute (USA) - Dr. Benjamin Good
                   Delphinai Corporation (Canada) / MedgenInformatics - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
from threading import Thread

from django.db.transaction import commit,rollback

# from django.db import connections
# print >>logger, connections.queries

from gbk.settings import DEBUG, logger
 
TEST  = False
TRACE = False

from query.datasource import DSID
from query import QueryFactory

from semmeddb.models import (
    Concept,
    Predication,
)

def auditor():
    
    print >>logger, "#### Starting SemMedDb Data Audit ####"
    
    query_engine = QueryFactory( DSID.SEMMEDDB )
    
    concepts = None  # empty variable
    try:
        concepts = Concept.objects.using( query_engine.getDsid() ).all()
        
    except Exception as e:
        print >>logger,e
        return  # gross data retrieval failure?
      
    n = 0
    
    for concept in concepts:
        
        n += 1
        
        if TEST:
            if n>100: break  # test run, terminate prematurely
            
        if TRACE:
            print >>logger, "Found Concept: ", concept.cui, " with name: ", concept.preferred_name

        try:
            # check if this concept has predications associated with it
            hits = Predication.objects.using( query_engine.getDsid() ).filter(
                        predicationargument__concept_semtype__concept__concept_id = concept.concept_id
                    ).distinct().count()
            
            if hits == 0: 
                # record orphan concept id for deletion at the end of the task
                if DEBUG:
                    print >>logger, "Orphan Concept: ", concept.cui, " with name: ", concept.preferred_name
                concept.is_orphan = True
            else:
                concept.is_orphan = False
            
            concept.save()
            commit( using= query_engine.getDsid() )
        
        except Exception as e:
            print >>logger,e # not sure what failure modes this script has...
            rollback( using= query_engine.getDsid() )

        if n%10 == 0:
            print >>logger,".", # progress dot...
        if n%100 == 0:
            print >>logger # newline every twenty dots
        if n%1000 == 0:
            print >>logger, "SemMedDb background data audit: ", n , " concepts audited so far..."

    print >>logger, "#### Completed SemMedDb Data Audit completed across ", n , " concepts ###"

    
def data_audit():
    if DEBUG:
        print >>logger, "#### Starting background thread for SemMedDb Data Audit ####"
    
    Thread(
        target=auditor, 
        name="SemMedDb_Data_Auditor", 
        kwargs={}
    ).start()

