## Introduction ##

SQL dumps of the SemMedDb and Implicitome data models used in Knowledg.Bio are located in the config directory of the project.
Be warned that, for practical reasons, such models differ slightly from their external public versions (see especially
the notes about Implicitome below).

## Implicitome Schema Changes ##

To better align the model field names to common primary and foreign field naming conventions in the SemMedDb portion of Knowledg.Bio (i.e. ‘*_id’ format), the Implicitome database primary and foreign keys were renamed across all of the tables to include an underscore character before the ‘id’ suffix.  Certain other fields were renamed either for Django compatibility. Here are the changes, along with the MySQL SQL used to make the adjustments:

* **concept** table: the concept_id field also needs to be ‘autoincremented’ in the concept table itself; (Jul 8, 2015) Added is_orphan field.

```
#!SQL

ALTER TABLE concept DROP PRIMARY KEY;
ALTER TABLE concept CHANGE COLUMN conceptid concept_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE concept ADD is_orphan BOOLEAN DEFAULT FALSE;

```

* **dblink** table: 

```
#!SQL

ALTER TABLE dblink DROP PRIMARY KEY ; 
ALTER TABLE dblink  CHANGE COLUMN dblinkid dblink_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
                    CHANGE COLUMN conceptid concept_id INT NOT NULL,
                    CHANGE COLUMN dbid db_id VARCHAR(4) NOT NULL,
                    CHANGE COLUMN id identifier VARCHAR (255);
```

* **term** table: in this table, the existing primary keys were dropped. The fields ‘termid’ renamed to ‘subterm_id’ and ‘conceptid’ renamed to ‘concept_id’, then a new autoincrement primary key (‘term_id’) added.

```
#!SQL

ALTER TABLE term DROP PRIMARY KEY; 
ALTER TABLE term CHANGE COLUMN conceptid concept_id INT NOT NULL,
                 CHANGE COLUMN termid subterm_id INT NOT NULL,
                 ADD UNIQUE (concept_id, subterm_id);
ALTER TABLE term ADD term_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;
```
* **relation** table: concept1_id and concept2_id ought to be foreign keys…but…

```
#!SQL

ALTER TABLE relation DROP PRIMARY KEY, 
ALTER TABLE relation CHANGE COLUMN relationid relation_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE relation CHANGE COLUMN conceptid1 concept1_id int NOT NULL,
                     CHANGE COLUMN conceptid2 concept2_id int NOT NULL,
                     ADD UNIQUE (concept1_id, concept2_id),
                     CHANGE COLUMN relationtypeid relationtype_id INT UNSIGNED NOT NULL;
```

* **tuples** table: all id’s already seem to be in the ‘*_id’ format, however, integration of the ABC model of concept linkage required the following column. Also, performance requirements suggests indexing of the sub_id and obj_id.

```
#!SQL

ALTER TABLE tuples ADD linked_concepts TEXT DEFAULT NULL; 

CREATE INDEX tuples_concept_pairs ON tuples(sub_id,obj_id);

```

## Addition of the UMLS Schema ##

June 15, 2015 saw the addition of a new UMLS database to manage UMLS Metathesaurus meta-data. The initial need here was for the MRDEF.RRF concept definitions.

To use this new functionity, the new 'umls' database needs to be created as follows:


```
#!SQL

CREATE DATABASE umls DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


```

and tables created by reading in the MRDEF.sql from the UMLS site. Be sure to GRANT access to the new umls database, to all relevant mysql user accounts.

After initial loading of the database, a modification of the resulting 'mrdef' table data model is required for Django compatibility, 
by applying the following SQL:

```
#!SQL

ALTER TABLE mrdef DROP PRIMARY KEY;
ALTER TABLE mrdef ADD DEFINITION_ID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;


```


