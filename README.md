# The Knowledge.Bio

# License #

The MIT License (MIT)

Copyright (c) 2015-16 Scripps Institute (USA) - Dr. Benjamin Good and 
                      STAR Informatics / Delphinai Corporation (Canada) - Dr. Richard Bruskiewich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

This project is a Django python wrapper for the ubuntu Knowledge.Bio access and use of a variety 
of semantic data sources for use in crowdsourcing refinement of biomedical knowledge.
* NCBI Semantic Medline Database (SemMedDb)
* Implicitome database (Hettne, K et al, BioSemantics group, Department of Human Genetics, Leiden University Medical Center)

## Setup

### Server Dependencies

* `sudo apt-get update`
* `sudo apt-get upgrade`
* `sudo apt-get install build-essential python python-dev python-pip python-virtualenv mysql-client libmysqlclient-dev git-core nginx libpcre3 libpcre3-dev`

### (Virtual) Working Environment

* Make a python virtual environment ... make sure it is locally owned
* `cd /opt; sudo virtualenv gbk-env`
* `sudo chown -R ubuntu:ubuntu gbk-env`

* ...and activate it. You should generally plan to install, configure and run the system from within the virtualenv (i.e. run 'activate' every day when logging in to work).
* `. /opt/gbk-env/bin/activate`

This will ensure that the default python on the path will be /opt/gbk-env/bin/python 
and its (locally installed) libraries.  If you forget to activate, the running of this
application will complain about missing libraries.

### Project Installation

* Make the project folder ('deploy' user account name suggested but can be whatever...) and download the repo
* `sudo adduser deploy`
* `sudo mkdir -p /home/deploy/webapps`
* `cd /home/deploy/webapps/`
* `sudo chown -R ubuntu:ubuntu /home/deploy/webapps/`

Set up your Git account locally first, then...

* `git clone https://USER@bitbucket.org/starinformatics/gbk.git && cd gbk`

* Install all the python related dependencies
* `sudo /opt/gbk-env/bin/pip install -r requirements.txt`

### Configuration of the Django Application

The gbk .gitignore actually suppresses the commit of local Django settings.py files, but 
a templated version of this file is committed thus should be copied into a local 'settings.py' file
in the src/gbk subdirectory and customized.

At this point, one only worries about customizing the database credentials in the settings.py file.
This can be done either by changing the os.getenv default values in the settings.py file,
or by setting operating system environment variables as follows:

DB_HOST - server IP address or hostname of the database
DB_ENGINE - database engine e.g. 'sqlite3' (for local testing) or 'mysql' (for access to the 'real' MySQL database, see below)
DB_NAME - database name e.g. 'semmeddb'
DB_USER - database user , e.g. 'root' or equivalent database user (for MySQL)
DB_PASSWORD - actual database password for database access (for MySQL)

As it happens, it may be trickier to set environment variables in the virtual environment of the uwsgi daemon 
process running the application, and since one has a local copy of settings.py (outside of the git repo), it
may be convenient to simply set the os.getenv default values to the desired settings.

## Permissions and Remote Access to Database ##

Local and remote access to the MySQL database requires that the bind-address property 
in the /etc/mysql/my.cnf file be properly set to the local (AWS Private) IP address of the server.
Note that this value is unique to every EC2 instance created to host the database.

Note that for proper testing of the database with MySQL, the user account must have MySQL table 'create' privileges.
The quick (but likely insecure) way is to set the user account to the 'root' account for MySQL, but time permitting,
a distinct project mysql user account (i.e. Knowledg.Bio) could be created and 'GRANT'ed the necessary privileges 
for remote access (see MySql documentation). I.E. (while logged into the MySQL root or equivalent account...)

```
#!SQL

        GRANT SELECT ON `implicitome`.* TO 'gbk'@'<your internet visible IP>'

```

You can also directly change the host name entries in the mysql.db and mysql.user tables 
(including the 'local' AWS ip-###-###-###-### address). After making such changes, 
restart the server (sudo /etc/init.d/mysql restart) and check visibility of the server
with a remote mysql client. If the mysql client sees it, then your application will too.

Make sure that your internet visible IP is what you think it is. Some internet service providers
may periodically change your assigned IP address. If this happens, in addition to the above,
you may need to fix your AWS firewall settings to ensure visibility of the database.

Finally, ensure that your settings.py file database configuration settings have been fixed accordingly.

### Database Initialization

The Knowledg.Bio accesses the Semantic Medline Database (SemMedDb) and Implicitome models 
defined in the 'semmeddb' and 'implicitome' applications, respectively. 
Since we are normally accessing a 'legacy' MySQL database, we won't generally need to 'migrate' the models.

Obviously, if you need a copy of the SemMedDb on your server, you need to install the MySQL server, i.e.

   sudo apt-get install mysql-server
   
setting a suitable user account and password (which you configure in settings.py to access the system)

However, if you are trying to develop and test the software locally, you will need
to (re)create the local database environment for testing.  

Assuming that such a database is properly configured in the 'gbk.settings.py' module, then:

* `sudo /opt/gbk-env/bin/python manage.py migrate`

will initialize the database locally for testing purposes.

Note that if you wish to add or modify the database models to reflect new schemata, you will need to define new migrations, e.g.

* `sudo /opt/gbk-env/bin/python manage.py makemigration semmeddb` 
* `sudo /opt/gbk-env/bin/python manage.py makemigration implicitome`

If you run into trouble running unit tests against your local copy of the database(s), you can try dropping 'gbk', 'semmeddb' and 'implicitome' then recreating the 'gbk' database (only) before attempting to run unit tests against the database.

### Implicitome Schemata Adjustments ###

The Implicitome schemata was modified slightly to adapt it to the Knowledg.Bio. These changes are documented in the wiki [here](https://bitbucket.org/starinformatics/gbk/wiki/Knowledg.Bio%20Data%20Model%20Schemata%20Modifications%20for%20Implicitome%20Database)

### Web Server Configuration

For the initial deployment, a more conventional uWSGI deployment process was used
for Knowledg.Bio than the gunicorn used by the original Mark2Cure 

Generally, the procedures is as documented at 

    https://uwsgi.readthedocs.org/en/latest/tutorials/Django_and_nginx.html

and (with additional details) at

    https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/uwsgi/    

using the NGINX configuration file in the 'config' directory of the gbk project
(symbolically linked as, or copied over to, '/etc/nginx/sites-enabled/default').

One probably needs to add the server user account to the www-data group, 
and add the www-data user to the server user account, to ensure access 
(perhaps need to review security of giving nginx www-data access to the user account).

The uWSGI installation procedure may be taken up to the INI file stage (excluding
the use of the 'emperor' mode for now) with the simple automatic startup of the uwsgi

     /usr/local/bin/uwsgi --ini /home/deploy/webapps/gbk/config/gbk_uwsgi.ini

inserted directly in the /etc/rc.local file. Note that running the uWSGI from
rc.local requires that the uid and gid of the ini file point to the NGINX user
and group (e.g. www-data). We assume that NGINX has already been started
as an init.d daemon.

Also, if you set your settings.py database parameters (see above) from environment 
variables, you should ensure that such values are still set and visible via the 
rc.local (probably not, by default... rc.local executes in the root account, 
and the uwsgi may only see on the NGINX www-data environment).

This is one instance where setting the values directly in the gbk.settings.py
may be convenient. If you do so, you may wish to set strict access permissions on
the settings.py file, to enforce security.

Along the way of configuring the machine, some steps may fail due to Unix permission
conflicts of various types. Use common sense to fix these 
(i.e. chown or delete created log files, etc.)

NOTES FOR PRODUCTION DEPLOYMENT
===============================

Django documentation has useful tips on production deployment. One thing to watch out for
is the resetting of the settings.DEBUG flag to False. Note that this changes the use 
of settings.py variables, which may affect the operation of the web site. In particular,
the ALLOWED_HOSTS variable must NOT be empty, but either have a wild-card '*' or a list of 
particular permitted domain names. Since NGINX usually does the proxy filtering of inputs,
in its 'server_name' field, it is perhaps OK to use the '*' wildcard in the settings.py file.

TODO: the uWSGI command should probably be invoked as a service in init.d rather than in rc.local
with a suitable script to allow graceful restarts.